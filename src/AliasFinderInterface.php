<?php

namespace Drupal\pathauto_export;

/**
 * An interface that defines a utility for finding aliases.
 */
interface AliasFinderInterface {

  /**
   * Get aliases filtered by entity type id.
   *
   * @param string $entityTypeId
   *   The id of the entity type.
   *
   * @return array
   *   Array of aliases.
   */
  public function getAliasesByEntityType(string $entityTypeId): array;

  /**
   * Get aliases filtered by entity type id and bundle id.
   *
   * @param string $entityTypeId
   *   The id of the entity type.
   * @param string $bundleId
   *   The bundle id.
   *
   * @return array
   *   Array of aliases.
   */
  public function getAliasesByBundle(string $entityTypeId, string $bundleId): array;

}
