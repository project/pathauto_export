<?php

namespace Drupal\pathauto_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\path_alias\AliasRepositoryInterface;
use Drupal\pathauto\AliasStorageHelperInterface;
use Drupal\pathauto\AliasTypeManager;
use Drupal\pathauto_export\AliasFinderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Pathauto Export Controller class.
 */
class PathautoExportController extends ControllerBase {

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Provides helper methods for accessing alias storage.
   *
   * @var \Drupal\pathauto\AliasStorageHelperInterface
   */
  protected $aliasStorageHelper;

  /**
   * The alias type manager.
   *
   * @var \Drupal\pathauto\AliasTypeManager
   */
  protected $aliasTypeManager;

  /**
   * The alias repository.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $aliasRepository;

  /**
   * The alias finder.
   *
   * @var \Drupal\pathauto_export\AliasFinderInterface
   */
  protected $aliasFinder;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\pathauto\AliasStorageHelperInterface $aliasStorageHelper
   *   Provides helper methods for accessing alias storage.
   * @param \Drupal\pathauto\AliasTypeManager $aliasTypeManager
   *   The alias type manager.
   * @param \Drupal\path_alias\AliasRepositoryInterface $aliasRepository
   *   The alias repository.
   * @param \Drupal\pathauto_export\AliasFinderInterface $aliasFinder
   *   The alias finder.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo, Connection $database, AliasStorageHelperInterface $aliasStorageHelper, AliasTypeManager $aliasTypeManager, AliasRepositoryInterface $aliasRepository, AliasFinderInterface $aliasFinder) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->database = $database;
    $this->aliasStorageHelper = $aliasStorageHelper;
    $this->aliasTypeManager = $aliasTypeManager;
    $this->aliasRepository = $aliasRepository;
    $this->aliasFinder = $aliasFinder;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection ReturnTypeCanBeDeclaredInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('database'),
      $container->get('pathauto.alias_storage_helper'),
      $container->get('plugin.manager.alias_type'),
      $container->get('path_alias.repository'),
      $container->get('pathauto_export.alias_finder')
    );
  }

  /**
   * Shows the download list.
   *
   * @return array
   *   A render array as expected by the renderer.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function list(): array {

    $headers = [
      $this->t('Alias type'),
      $this->t('Count'),
      $this->t('Bundles'),
      $this->t('Export link'),
    ];
    $rows = [];

    // First we do the "all" case.
    $row = [];
    $row['aliases'] = $this->t('All aliases');
    $row['count'] = $this->aliasStorageHelper->countAll();
    $row['bundles']['data'] = '--';
    $row['download']['data'] = [
      '#type' => 'inline_template',
      '#template' => '<a href="' . Url::fromRoute('pathauto_export.admin.download', [
        'prefix' => 'all',
      ])->toString() . '" target="_blank">' . $this->t('Download') . '</a>',
    ];
    $rows[] = $row;

    // Next, iterate over all visible alias types.
    $definitions = $this->aliasTypeManager->getVisibleDefinitions();
    foreach ($definitions as $id => $definition) {
      $entityTypeId = str_replace('canonical_entities:', '', $id);
      /** @var \Drupal\pathauto\AliasTypeInterface $aliasType */
      $aliasType = $this->aliasTypeManager->createInstance($id);
      $count = $this->aliasStorageHelper->countBySourcePrefix($aliasType->getSourcePrefix());
      $row = [];
      $row['aliases'] = $this->t((string) $definition['label']);
      $row['count'] = $count;
      $row['bundles']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<a href="' . Url::fromRoute('pathauto_export.admin.bundle.list', [
          'entityTypeId' => $entityTypeId,
        ])->toString() . '">' . $this->t('List bundles') . '</a>',
      ];
      $row['download']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<a href="' . Url::fromRoute('pathauto_export.admin.download', [
          'prefix' => $entityTypeId,
        ])->toString() . '" target="_blank">' . $this->t('Download') . '</a>',
      ];

      $rows[] = $row;
    }

    $headerMessage = $this->t(
      'Choose aliases to download'
    );

    $list['download_list_header'] = [
      '#markup' => '<p>' . $headerMessage . '</p>',
    ];

    $list['download_list'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    ];

    return $list;
  }

  /**
   * Shows the bundle list.
   *
   * @param string $entityTypeId
   *   The entity type id to be listed.
   *
   * @return array
   *   A render array as expected by the renderer.
   */
  public function listBundle(string $entityTypeId): array {

    $definitions = $this->aliasTypeManager->getVisibleDefinitions();
    $validEntityTypeIds = [];
    foreach ($definitions as $id => $definition) {
      $validEntityTypeIds[] = str_replace('canonical_entities:', '', $id);
    }

    if (!in_array($entityTypeId, $validEntityTypeIds, TRUE)) {
      throw new NotFoundHttpException();
    }

    $headers = [
      $this->t('Bundle'),
      $this->t('Export link'),
    ];
    $rows = [];

    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
    foreach ($bundles as $bundleId => $bundle) {
      $row = [];
      $row['bundle'] = $this->t((string) $bundle['label']);
      $row['download']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<a href="' . Url::fromRoute('pathauto_export.admin.bundle.download', [
          'entityTypeId' => $entityTypeId,
          'bundleId' => $bundleId,
        ])->toString() . '" target="_blank">' . $this->t('Download') . '</a>',
      ];
      $rows[] = $row;
    }

    $headerMessage = $this->t(
      'Choose aliases to download'
    );

    $list['download_list_header'] = [
      '#markup' => '<p>' . $headerMessage . '</p>',
    ];

    $list['download_list'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    ];

    return $list;
  }

  /**
   * Download a CSV file with alias.
   *
   * @param string $prefix
   *   The id of the alias type.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The redirects.csv file as a response object with csv format.
   */
  public function download(string $prefix): Response {
    $validAliases = $this->aliasFinder->getAliasesByEntityType($prefix);

    $content = '"PATH","ALIAS","LANGCODE"' . "\n";
    foreach ($validAliases as $alias) {
      $content .= '"' . $alias->path . '","' . $alias->alias . '","' . $alias->langcode . '"' . "\n";
    }

    $response = new Response($content);
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'aliases.' . $prefix . '.csv'
    );
    $response->headers->set('Content-Type', 'text/csv; utf-8');
    $response->headers->set('Content-Disposition', $disposition);
    return $response;
  }

  /**
   * Download a CSV file with alias.
   *
   * @param string $entityTypeId
   *   The entity type id to be listed.
   * @param string $bundleId
   *   The bundle id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The redirects.csv file as a response object with csv format.
   */
  public function downloadBundle(string $entityTypeId, string $bundleId): Response {
    $aliasesByBundle = $this->aliasFinder->getAliasesByBundle($entityTypeId, $bundleId);
    $content = '"PATH","ALIAS","LANGCODE"' . "\n";
    foreach ($aliasesByBundle as $alias) {
      $content .= '"' . $alias->path . '","' . $alias->alias . '","' . $alias->langcode . '"' . "\n";
    }

    $response = new Response($content);
    $disposition = $response->headers->makeDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'aliases.' . $entityTypeId . '.' . $bundleId . '.csv'
    );
    $response->headers->set('Content-Type', 'text/csv; utf-8');
    $response->headers->set('Content-Disposition', $disposition);
    return $response;
  }

}
