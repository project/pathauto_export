<?php

namespace Drupal\pathauto_export;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a utility for finding aliases.
 */
class AliasFinder implements AliasFinderInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.   *.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $entityTypeBundleInfo) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function getAliasesByEntityType(string $entityTypeId): array {
    $aliases = [];
    $queryStatement = $this->database->select('path_alias')
      ->fields('path_alias');
    if ($entityTypeId !== 'all') {
      $queryStatement->condition('path', '/' . str_replace("_", "/", $entityTypeId) . '/%', 'LIKE');
    }
    $query = $queryStatement
      ->orderBy('id', 'DESC')
      ->execute();
    if ($query) {
      $aliases = $query->fetchAll();
    }

    // Use only newest alias.
    $validAliases = [];
    foreach ($aliases as $alias) {
      if (!isset($validAliases[$alias->path])) {
        $validAliases[$alias->path] = $alias;
      }
    }
    // Restore aliases original order.
    sort($validAliases);

    return $validAliases;
  }

  /**
   * {@inheritdoc}
   */
  public function getAliasesByBundle(string $entityTypeId, string $bundleId): array {
    $aliases = [];
    $aliasesByEntityType = $this->getAliasesByEntityType($entityTypeId);
    $conditionFieldByEntityTypeId = [
      'node' => 'type',
      'taxonomy_term' => 'vid',
      'media' => 'bundle',
    ];

    if (isset($conditionFieldByEntityTypeId[$entityTypeId])) {
      $idsByBundle = \Drupal::entityQuery($entityTypeId)
        ->condition($conditionFieldByEntityTypeId[$entityTypeId], $bundleId)
        ->accessCheck(FALSE)
        ->execute();
      foreach ($aliasesByEntityType as $alias) {
        $pathParts = explode('/', $alias->path);
        $entityId = array_pop($pathParts);
        if (in_array($entityId, $idsByBundle, TRUE)) {
          $aliases[] = $alias;
        }
      }
    }

    return $aliases;
  }

}
