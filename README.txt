Pathauto export

A simple module that exports aliases from pathauto module into a CSV file.

Aliases can be downloaded by type (node, media, etc) or by bundle (node page, node article, etc) or all of them at once.
